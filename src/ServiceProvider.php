<?php

namespace cieel\CieelTheme;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\View;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(
        Factory $view,
        Dispatcher $events,
        Repository $config
    ) {
        $this->loadViews();
        $this->loadTranslations();
        $this->publishAssets();
        $this->publishConfig();

        // config
        View::share('title', config('laravel-cieel-theme.title'));
        View::share('menu', config('laravel-cieel-theme.menu'));
        View::share('dashboard_url', config('laravel-cieel-theme.dashboard_url'));
        View::share('logout_method', config('laravel-cieel-theme.logout_method'));
        View::share('login_url', config('laravel-cieel-theme.login_url'));
        View::share('logout_url', config('laravel-cieel-theme.logout_url'));
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function packagePath($path)
    {
        return __DIR__."/../$path";
    }


    /* Carrega os arquivos para pasta VIEWS */
    private function loadViews()
    {
        $viewsPath = $this->packagePath('resources/views');
        $this->loadViewsFrom($viewsPath, 'laravel-cieel-theme');
        $this->publishes([
            $viewsPath => resource_path('views'),
        ], 'views');
    }

    private function loadTranslations()
    {
        $translationsPath = $this->packagePath('resources/lang');
        $this->loadTranslationsFrom($translationsPath, 'laravel-cieel-theme');
        $this->publishes([
            $translationsPath => resource_path('resources/lang/'),
        ], 'translations');
    }

    private function publishAssets()
    {
        $this->publishes([
            $this->packagePath('resources/assets') => public_path('assets'),
        ], 'assets');
    }

    private function publishConfig()
    {
        $configPath = $this->packagePath('config/laravel-cieel-theme.php');
        $this->publishes([
            $configPath => config_path('laravel-cieel-theme.php'),
        ], 'config');
        $this->mergeConfigFrom($configPath, 'laravel-cieel-theme');
    }

}
