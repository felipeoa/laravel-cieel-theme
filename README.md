# laravel-cieel-theme

Esse pacote laravel é um template com alguns estilos da USP. É direcionado para uso dos Sistemas da Escola de Engenharia de Lorena - USP. Foi inspirado no laravel-usp-theme.

Requisitos
Este tema foi testado no Laravel 5.6.x mas deve funcionar em outras versões.

Ele é baseado no bootstrap 4.3.x então todos os estilos dele estão disponíveis.

Criação: Mario Danilo - mdsilva@usp.br 03/07/2019

Alteração: Dev CI/EEL - dev.ci.eel@usp.br 01/03/2020 

Instalação

Para instalar o tema, use o composer:

    composer require devcieel/laravel-cieel-theme

Publique os assets:

    php artisan vendor:publish --provider="cieel\CieelTheme\ServiceProvider" --tag=assets --force

Publique as views:

    php artisan vendor:publish --provider="cieel\CieelTheme\ServiceProvider" --tag=views --force    

E por fim os arquivos de configuração:

    php artisan vendor:publish --provider="cieel\CieelTheme\ServiceProvider" --tag=config

Edite o arquivo com as variáveis de seu ambiente:

    config/laravel-cieel-theme.php

Nesse arquivo a variável 'can' => 'admin' controla a disponibilidade dos itens do menu conforme os gates configurados na aplicação. Se can estiver vazio, 'can' => '', o menu será exibido sempre. Para que o menu apareça somente para o gate admin por exemplo, use 'can' => 'admin'.

Seções disponíveis:

* title
* content

Seções para css e javascript. É uma boa ideia usar {{ parent() }} para herdar os css/js default:

* styles
* javascripts_head
* javascripts_bottom


Exemplo básico:

@extends('layouts.master') ou @extends('layouts.form')

@section('title') CI-EEL @endsection

@section('content')
    Seu código
@endsection
